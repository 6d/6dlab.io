+++
title = "我们的团队"
weight = 30
draft = false
+++

{{< figure class="image main" src="/images/team.jpg" >}}

> 沟通协作，专业高效

我们的团队是一个固定的团队，而不是一个临时的团伙，我们坚信只有经过多年的磨合，团队才能拥有共同的价值观、服务理念、做工方式，才能更好的服务大众。

## 工长：陈明伟

![](http://bjcache.leju.com/gongzhang/Chief/5b/14/7540894bf270d64b39d407c8a5bd_thumb_120x160.jpg)

- 从艺20余年
- [手艺现场视频](http://v.youku.com/v_show/id_XMjQ3NDU0OTMyMA==.html?spm=a2h0k.8191407.0.0&from=s1.8-1-1.2)

## 设计师：万娇

- 擅长别墅设计
- xxx

## 泥水匠：xxx

## 水电工：xxx



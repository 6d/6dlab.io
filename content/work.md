+++
title = "我们的流程"
weight = 20
draft = false
+++

{{< figure class="image main" src="/images/pic02.jpg" >}}

> 精益求精，绝不将就

我们一直在不断的优化装修流程以提高装修效率和质量，在我们的流程中有很多特色工艺以及一些我们一直坚持的标准，只为让每次装修都趋近完美。

## 第一步：房屋测量

xxx

## 第二步：户型设计

xxx

## 第三步：xxx

xxx

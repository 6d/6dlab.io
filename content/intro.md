+++
title = "我们的服务"
weight = 10
draft = false
+++

{{< figure class="image main" src="/images/pic01.jpg" >}}

> 立足重庆，服务大众

我们提供基装和整装服务，如果你想省时省力整装服务是你最好的选择；如果你想要个性化的装修自己的家就选择我们的基装服务吧！以下是我们提供的材料，点击链接可以获取材料相关的图片、质量、价格、环保等相关信息，供大家参考。

## 基装材料

- [伟星线管](https://item.jd.com/1321156672.html)
- xxx

## 整装材料

- [80x80马可波罗瓷砖](https://item.jd.com/10482917810.html)
- xxx